<?php

require 'vendor/autoload.php';
use App\HttpState;
use App\HtmlDataGrid;
use App\DefaultConfig;

$filename = 'data.json';


set_error_handler(
    function ($severity, $message, $file, $line) {
        throw new ErrorException($message, $severity, $severity, $file, $line);
    }
);

try {
    file_get_contents($filename);
}
catch (Exception $e) {
    echo $e->getMessage();
}

restore_error_handler();

$rows = file_exists($filename) ? json_decode(file_get_contents($filename), true) : [];

$state = HttpState::create(); // instanceof State, dane powinny zostać pobrane z $_GET

$dataGrid = new HtmlDataGrid(); // instanceof DataGrid
$config = (new DefaultConfig) // instanceof Config, z dodatkowymi metodami
    ->addIntColumn('id')
    ->addTextColumn('name')
    ->addIntColumn('age')
    ->addTextColumn('company')
    ->addCurrencyColumn('balance', 'USD')
    ->addTextColumn('phone')
    ->addTextColumn('email');

echo $dataGrid->withConfig($config)->render($rows, $state);