<?php

namespace App\Columns;

require 'vendor/autoload.php';
use App\Column;
use App\DataType;

class CurrencyColumn implements Column {
    private $label;
    private $type;
    private $align;
    public function withLabel(string $label): Column {
        $this->label = $label;
        return $this;
    }

    public function getLabel(): string {
        return $this->label;
    }

    public function withDataType(DataType $type): Column {
        $this->type = $type;
        return $this;
    }

    public function getDataType(): DataType {
       return $this->type;
    }

    public function withAlign(string $align): Column {
        $this->align = $align;
        return $this;
    }

    public function getAlign(): string {
        if(is_null($this->align)) {
            return '';
        }
        return $this->align;
    }
    public function sortData(&$rows, $sort, $orderAsc): array {
        usort($rows, function ($a, $b) use ($sort, $orderAsc) {
                if($orderAsc) {
                    return $a[$sort] - $b[$sort];
                }
                return $b[$sort] - $a[$sort];
        });
        return $rows;
    }
}