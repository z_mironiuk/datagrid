<?php

namespace App;

require 'vendor/autoload.php';
use App\DataGrid;
use App\Config;
use App\State;

    
class HtmlDataGrid implements DataGrid {
    
    public $config;
    public function withConfig(Config $config): DataGrid {
        $this->config = $config;
        return $this;
    }

    /**
     * Renderuje na ekran kod, który ma za zadanie wyświetlić przygotowany DataGrid.
     * Jako parametr przyjmuje: wszystkie dostępne dane, oraz aktualny stan DataGrid w formie obiektu - State.
     * Na podstawie State, metoda ma za zadanie posortować wiersze oraz podzielić je na strony.
     */
    public function render(array $rows, State $state): void {
        $columns = $this->config->getColumns();
        $sort = $state->getOrderBy();
        $orderAsc = $state->isOrderAsc();
        $orderDesc = $state->isOrderDesc();
        $currentPage = $state->getCurrentPage();
        $rowsPerPage = $state->getRowsPerPage();
        $arrowUp = '<svg class="bi bi-arrow-up-short" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M8 5.5a.5.5 0 0 1 .5.5v5a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5z"/>
                <path fill-rule="evenodd" d="M7.646 4.646a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1-.708.708L8 5.707 5.354 8.354a.5.5 0 1 1-.708-.708l3-3z"/>
            </svg>';
        $arrowDown = '<svg class="bi bi-arrow-down-short" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                <path fill-rule="evenodd" d="M4.646 7.646a.5.5 0 0 1 .708 0L8 10.293l2.646-2.647a.5.5 0 0 1 .708.708l-3 3a.5.5 0 0 1-.708 0l-3-3a.5.5 0 0 1 0-.708z"/>
                <path fill-rule="evenodd" d="M8 4.5a.5.5 0 0 1 .5.5v5a.5.5 0 0 1-1 0V5a.5.5 0 0 1 .5-.5z"/>
            </svg>';
        $arrow = $orderAsc ? $arrowDown : $arrowUp;
        $sortingColumn;
        echo '<!DOCTYPE html>
                <html lang="en">
                <head>
                  <title>Table</title>
                  <meta charset="utf-8">
                  <meta name="viewport" content="width=device-width, initial-scale=1">
                  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
                    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
                  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
                  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
                </head>
                <body>';
        
        echo '<div class="container">';
        echo '<table class="table table-bordered">';
        $headerLinks = '<th class="th-sm"><a href="index.php?orderBy=%s&orderAsc=%s&orderDesc=%s">%s %s</a></th>';
        foreach($this->config->getColumns() as $column) {
            if($column->getLabel() == $sort) {
                $sortingColumn = $column;
                echo sprintf($headerLinks, $column->getLabel(), !$orderAsc, !$orderDesc, $column->getLabel(), $arrow);
            }
            else {
                echo sprintf($headerLinks, $column->getLabel(), true, false, $column->getLabel(), '');
            }
        }
        
        $sortingColumn->sortData($rows, $sortingColumn->getLabel(), $orderAsc);
        $pages = ceil(count($rows) / $rowsPerPage);
        $rows = array_slice($rows, ($currentPage-1)*$rowsPerPage, $rowsPerPage);
        
        
        foreach($rows as $row) {
            echo '<tr>';
                foreach($this->config->getColumns() as $column) {
                    if(empty($column->getAlign())) {
                    echo sprintf('<td>%s</td>', $column->getDataType()->format($row[$column->getLabel()]));
                    }
                    else {
                        echo sprintf('<td style="text-align: %s;">%s</td>', $column->getAlign(), $column->getDataType()->format($row[$column->getLabel()]));
                    }
                }
            echo '</tr>';
        }
        echo '</table>';
        $link = sprintf('index.php?orderBy=%s&orderAsc=%s&orderDesc=%s', $sort, $orderAsc, $orderDesc);
        echo '<nav aria-label="...">
                <ul class="pagination">';
                    for($page=1; $page<=$pages; $page++) {
                        $pageLink = sprintf('%s&page=%s', $link, $page);
                        if($currentPage == $page) {
                            echo sprintf('<li class="page-item active">
                            <a class="page-link" href="%s"> %s <span class="sr-only">(current)
                            </span></a>
                            </li>', $pageLink, $page);
                        }
                        else {
                            echo sprintf('<li class="page-item"><a class="page-link" href="%s">%s
                            </a></li>', $pageLink, $page);
                        }
                    }
        echo '</ul>
                </nav>';
        echo '</div>
        </body>
        </html>';
    }
}
    
?>
    
