<?php 

namespace App;

require 'vendor/autoload.php';
use App\Config;
use App\Column;
use App\DataTypes\NumberType;
use App\DataTypes\TextType;
use App\DataTypes\MoneyType;
use App\Columns\IntColumn;
use App\Columns\TextColumn;
use App\Columns\CurrencyColumn;

class DefaultConfig implements Config {
    
    private $columnList = array();
    
    public function addColumn(string $key, Column $column): Config {
        array_push($this->columnList, $column->withLabel($key));
        return $this;
    }
    public function getColumns(): array {
        return $this->columnList;
    }
    
    public function addIntColumn(string $key) {
        $column = (new IntColumn())->withDataType(new NumberType())->withAlign('right');
        $this->addColumn($key, $column);
        return $this;
    }
    public function addTextColumn(string $key) {
        $column = (new TextColumn())->withDataType(new TextType());
        $this->addColumn($key, $column);
        return $this;
    }
    public function addCurrencyColumn(string $key, string $value) {
        $column = (new CurrencyColumn())->withDataType(new MoneyType())->withAlign('right');
        $this->addColumn($key, $column);
        return $this;
    }
}
