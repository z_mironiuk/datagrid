<?php

namespace App;

require 'vendor/autoload.php';
use App\State;

class HttpState implements State {
    public static function create() {
        $state = new HttpState();
        return $state;
    }
    public function getCurrentPage(): int {
        $currentPage = isset($_GET['page']) ? $_GET['page'] : 1;
        return $currentPage;
    }
    public function getOrderBy(): string {
        $orderBy = isset($_GET['orderBy']) ? $_GET['orderBy'] : '';
        return $orderBy;
    }
    public function isOrderDesc(): bool {
        $orderDesc = isset($_GET['orderDesc']) ? $_GET['orderDesc'] : false;
        return $orderDesc;
    }
    public function isOrderAsc(): bool {
        $orderAsc = isset($_GET['orderAsc']) ? $_GET['orderAsc'] : true;
        return $orderAsc;
    }
    public function getRowsPerPage(): int {
        return 9;
    }
}