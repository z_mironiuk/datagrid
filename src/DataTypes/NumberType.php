<?php

namespace App\DataTypes;

require 'vendor/autoload.php';
use App\DataType;

class NumberType implements DataType {
    
    private $decimals;
    private $decimalSeparator;
    private $thousandsSeparator;
    
    public function __construct(int $decimals = 0, string $decimalSeparator = ',', string $thousandsSeparator = ' '){
        $this->decimals = $decimals;
        $this->decimalSeparator = $decimalSeparator;
        $this->thousandsSeparator = $thousandsSeparator;
    }
    
    public function format(string $value): string {
        return number_format($value, $this->decimals, $this->decimalSeparator, $this->thousandsSeparator);
    }
}