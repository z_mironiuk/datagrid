<?php

namespace App\DataTypes;

require 'vendor/autoload.php';
use App\DataType;

class MoneyType implements DataType {
    
    private $decimals;
    private $decimalSeparator;
    private $thousandsSeparator;
    private $currency;
    
    public function __construct(int $decimals = 0, string $decimalSeparator = ',', string $thousandsSeparator = ' ', string $currency = 'USD'){
        $this->decimals = $decimals;
        $this->decimalSeparator = $decimalSeparator;
        $this->thousandsSeparator = $thousandsSeparator;
        $this->currency = $currency;
    }
    
    public function format(string $value): string {
        return sprintf(number_format($value, $this->decimals, $this->decimalSeparator, $this->thousandsSeparator).' %s', $this->currency);
    }
}