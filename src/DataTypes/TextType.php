<?php

namespace App\DataTypes;

require 'vendor/autoload.php';
use App\DataType;

class TextType implements DataType {    
    public function format(string $value): string {
        return htmlspecialchars($value);
    }
}